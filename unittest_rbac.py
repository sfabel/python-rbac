import unittest
import ldap.extop.rbac.rbac_utils as rbac
from ldap.extop.rbac.requests import *
from ldap.extop.rbac.responses import *
import ldap
import pdb

"""Unit tests for RBAC functions"""

class TestRbacFunctions(unittest.TestCase):

    def setUp(self):
        #set up connection
        self.conn = ldap.initialize("ldap://vm-1-208.coe.hawaii.edu:389")
        self.conn.simple_bind_s("cn=admin,dc=coe,dc=hawaii,dc=edu","secret")

        sessRequest = CreateSessionRequest(uIdent="demoUser4",authTok="password")
        sessRequest.requestValue = sessRequest.encodedRequestValue()
        result = self.conn.extop_s(extreq=sessRequest)
        sessResponse = CreateSessionResponse(result[1])
        sessResponse.responseValue = sessResponse.decodeResponseValue()
        self.key = sessResponse.responseValue

    """Try to create  session with a user and the wrong password"""
    def test_session_key(self):
        self.assertTrue(type(self.key) == str)

        #negative case
        sessRequest = CreateSessionRequest(uIdent="demoUser4",authTok="passwd")
        sessRequest.requestValue = sessRequest.encodedRequestValue()

        error_msg = 'rbac_create_session: invalid credential'
        error = rbac.rbac_error_factory(error_msg)

        try:
            self.conn.extop_s(extreq=sessRequest)
        except Exception,e:
            self.assertTrue(e.__class__.__name__ == 'RBAC_CREATE_SESSION')
            self.assertTrue(e.message=='invalid credential')

    """Check if roles are activated properly in CreateSession"""
    def test_create_session_roles(self):
        #drop current session
        dropSessionRequest = DropSessionRequest(user="demoUser4", sessionID = self.key)
        dropSessionRequest.requestValue = dropSessionRequest.encodedRequestValue()
        self.conn.extop_s(extreq=dropSessionRequest)

        #role1 is activated
        sessRequest = CreateSessionRequest(uIdent="demoUser4",authTok="password", roles=['role1'])
        sessRequest.requestValue = sessRequest.encodedRequestValue()
        result = self.conn.extop_s(extreq=sessRequest)

        sessResponse = CreateSessionResponse(result[1])
        sessResponse.responseValue = sessResponse.decodeResponseValue()
        key = sessResponse.responseValue
        # print "test session key:  %s" % key
        self.assertTrue(type(key) == str)
        # print result

        #check that only role1 is activated
        sessionRolesRequest = SessionRolesRequest(user='demoUser4',sessionID=key)
        sessionRolesRequest.requestValue = sessionRolesRequest.encodedRequestValue()
        result = self.conn.extop_s(extreq=sessionRolesRequest)

        sessionRolesResponse = SessionRolesResponse(result[1])
        sessionRolesResponse.responseValue = sessionRolesResponse.decodeResponseValue()
        response = sessionRolesResponse.responseValue

        self.assertTrue('role1' in response)
        self.assertFalse('enmassesuperuser' in response)

        #try to activate bogus role
        sessRequest = CreateSessionRequest(uIdent="demoUser4",authTok="password", roles=['test'])
        sessRequest.requestValue = sessRequest.encodedRequestValue()

        error_msg = 'rbac_create_session: failed to assign roles to session'
        error = rbac.rbac_error_factory(error_msg)

        try:
            self.conn.extop_s(extreq=sessRequest)
        except Exception,e:
            self.assertTrue(e.__class__.__name__ == 'RBAC_CREATE_SESSION')
            self.assertTrue(e.message=='failed to assign roles to session')

    """Create the CheckAccessRequest depending on the opName and an optional objId"""
    def create_check_access_request(self,opName,objId=None):
        objName = '/cal/cal2.jsp'
        checkAccessRequest = CheckAccessRequest(self.key,opName,objName,objId)
        checkAccessRequest.requestValue = checkAccessRequest.encodedRequestValue()
        return checkAccessRequest

    """Returns the CheckAccessResponse depending on the result"""
    def get_access_response(self,result):
        accessResponse = CheckAccessResponse(result[1])
        accessResponse.responseValue = accessResponse.decodeResponseValue()
        response = accessResponse.responseValue
        return response

    """
        Test 1: user has access
        Test 2: user doesn't have access
        Test 3: user has permission, but objId is bogus (and therefore no permission)
    """
    def test_check_access(self):
        import pdb
        #positive case
        checkAccessRequest = self.create_check_access_request('8am')
        result = self.conn.extop_s(extreq=checkAccessRequest)
        response = self.get_access_response(result)
        self.assertTrue(response)

        #negative case
        checkAccessRequest = self.create_check_access_request('9am')
        result = self.conn.extop_s(extreq=checkAccessRequest)
        response = self.get_access_response(result)
        self.assertFalse(response)

        #negative case with bogus objId
        checkAccessRequest = self.create_check_access_request('8am', objId="test")
        result = self.conn.extop_s(extreq=checkAccessRequest)
        response = self.get_access_response(result)
        self.assertFalse(response)

    """Check to see if demoUser4 has role1 and enmassesuperuser"""
    def test_get_session_roles(self):
        #positive case
        sessionRolesRequest = SessionRolesRequest(user='demoUser4',sessionID=self.key)
        sessionRolesRequest.requestValue = sessionRolesRequest.encodedRequestValue()

        result = self.conn.extop_s(extreq=sessionRolesRequest)
        sessionRolesResponse = SessionRolesResponse(result[1])
        sessionRolesResponse.responseValue = sessionRolesResponse.decodeResponseValue()
        response = sessionRolesResponse.responseValue

        self.assertTrue('role1' in response)
        self.assertTrue('enmassesuperuser' in response)

    """Add role to session"""
    def test_add_active_role(self):
        dropActiveRoleRequest = DropActiveRoleRequest(user="demoUser4", session=self.key, activeRole="role1")
        dropActiveRoleRequest.requestValue = dropActiveRoleRequest.encodedRequestValue()
        self.conn.extop_s(extreq=dropActiveRoleRequest)

        #add role1 back
        addActiveRoleRequest = AddActiveRoleRequest(user="demoUser4", session=self.key,activeRole="role1")
        addActiveRoleRequest.requestValue = addActiveRoleRequest.encodedRequestValue()
        result = self.conn.extop_s(extreq=addActiveRoleRequest)

        self.assertTrue(result[1] == None)

        # check that it is added
        sessionRolesRequest = SessionRolesRequest(user='demoUser4',sessionID=self.key)
        sessionRolesRequest.requestValue = sessionRolesRequest.encodedRequestValue()
        result = self.conn.extop_s(extreq=sessionRolesRequest)

        sessionRolesResponse = SessionRolesResponse(result[1])
        sessionRolesResponse.responseValue = sessionRolesResponse.decodeResponseValue()
        response = sessionRolesResponse.responseValue

        self.assertTrue('enmassesuperuser' in response)
        self.assertTrue('role1' in response)

    """Try to add a bogus active role"""
    def test_add_bogus_active_role(self):
        addActiveRoleRequest = AddActiveRoleRequest(user="demoUser4", session=self.key,activeRole="role12")
        addActiveRoleRequest.requestValue = addActiveRoleRequest.encodedRequestValue()

        try:
            self.conn.extop_s(extreq=addActiveRoleRequest)
        except Exception,e:
            self.assertTrue(e.__class__.__name__ == 'RBAC_ADD_ACTIVE_ROLE')
            self.assertTrue(e.message == 'role not assigned to the user')

    """Try to add an active role to a user that doesn't own the sesssion"""
    def test_add_active_role_bogus_user(self):
        addActiveRoleRequest = AddActiveRoleRequest(user="demoUser100", session=self.key,activeRole="role1")
        addActiveRoleRequest.requestValue = addActiveRoleRequest.encodedRequestValue()
        try:
            self.conn.extop_s(extreq=addActiveRoleRequest)
        except Exception,e:
            self.assertTrue(e.__class__.__name__ == 'RBAC_ADD_ACTIVE_ROLE')
            self.assertTrue(e.message == 'unable to read user entry')

    """Drop session"""
    def test_drop_session(self):
        dropSessionRequest = DropSessionRequest(user="demoUser4", sessionID = self.key)
        dropSessionRequest.requestValue = dropSessionRequest.encodedRequestValue()
        result = self.conn.extop_s(extreq=dropSessionRequest)

        self.assertTrue(result[1] == None)

    """Try to drop a session for a user that doesn't own the session"""
    def test_drop_fake_session(self):
        dropSessionRequest = DropSessionRequest(user="demoUser5", sessionID = self.key)
        dropSessionRequest.requestValue = dropSessionRequest.encodedRequestValue()

        try:
            self.conn.extop_s(extreq=dropSessionRequest)
        except Exception,e:
            self.assertTrue(e.__class__.__name__ == 'RBAC_DELETE_SESSION')
            self.assertTrue(e.message == 'session not owned by user')

    """Successfully drop an active role"""
    def test_drop_active_role(self):
        dropActiveRoleRequest = DropActiveRoleRequest(user="demoUser4", session=self.key, activeRole="role1")
        dropActiveRoleRequest.requestValue = dropActiveRoleRequest.encodedRequestValue()
        result = self.conn.extop_s(extreq=dropActiveRoleRequest)

        self.assertTrue(result[1] == None)

        sessionRolesRequest = SessionRolesRequest(user='demoUser4',sessionID=self.key)
        sessionRolesRequest.requestValue = sessionRolesRequest.encodedRequestValue()
        result = self.conn.extop_s(extreq=sessionRolesRequest)

        sessionRolesResponse = SessionRolesResponse(result[1])
        sessionRolesResponse.responseValue = sessionRolesResponse.decodeResponseValue()
        response = sessionRolesResponse.responseValue

        self.assertTrue('enmassesuperuser' in response)
        self.assertFalse('role1' in response)

    """Attempt to drop a role that doesn't exist in the session"""
    def test_drop_bogus_active_role(self):
        dropActiveRoleRequest = DropActiveRoleRequest(user="demoUser4", session=self.key, activeRole="role2")
        dropActiveRoleRequest.requestValue = dropActiveRoleRequest.encodedRequestValue()
        try:
            self.conn.extop_s(extreq=dropActiveRoleRequest)
        except Exception,e:
            self.assertTrue(e.__class__.__name__ == 'RBAC_DROP_ACTIVE_ROLE')
            self.assertTrue(e.message == 'role not assigned to session')

    """Attempt to drop the session with a user that doesn't own the session"""
    def test_drop_active_role_bogus_user(self):
        dropActiveRoleRequest = DropActiveRoleRequest(user="demoUser15", session=self.key, activeRole="role1")
        dropActiveRoleRequest.requestValue = dropActiveRoleRequest.encodedRequestValue()
        try:
            self.conn.extop_s(extreq=dropActiveRoleRequest)
        except Exception,e:
            self.assertTrue(e.__class__.__name__ == 'RBAC_DROP_ACTIVE_ROLE')
            self.assertTrue(e.message == 'user not owner of the session')

    def tearDown(self):
        self.conn.unbind()

#show methods tested
suite = unittest.TestLoader().loadTestsFromTestCase(TestRbacFunctions)
unittest.TextTestRunner(verbosity=2).run(suite)