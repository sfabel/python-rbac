from ldap.extop import ExtendedRequest
from pyasn1.type import namedtype,univ,tag
import ldap.extop.rbac.rbac_utils as rbac
from pyasn1.codec.ber import encoder

"""
    Follows the ASN.1 description of this operation

    RBACCheckAccessRequestValue ::= SEQUENCE{
        sessionID   [0] OCTET STRING
        opName      [1] OCTET STRING
        objName     [2] OCTET STRING
        objId       [3] OCTET STRING OPTIONAL
    }
"""

class CheckAccessRequest(ExtendedRequest):
    requestName = rbac.op_map[rbac.CHECK_ACCESS]

    def __init__(self,sessionID,opName,objName,objId=None):
        self.sessionID = sessionID
        self.opName = opName
        self.objName = objName
        self.objId = objId
        self.args = [sessionID,opName,objName,objId]

    def createRequestValue(self):
        request_params = (namedtype.NamedType('sessionID',
                univ.OctetString().subtype(
                    implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple,0)
                )
            ),
            namedtype.NamedType('opName',
                univ.OctetString().subtype(
                    implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple,1)
                )
            ),
            namedtype.NamedType('objName',
                univ.OctetString().subtype(
                    implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 2)
                )
            ))
        if self.objId:
            objId_param = (namedtype.NamedType('objId',
                univ.OctetString().subtype(
                    implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 3)
                )
            ),)
            request_params = request_params + objId_param

        componentType = namedtype.NamedTypes(*request_params)
        sequence = univ.Sequence(componentType=componentType)

        return sequence

    def encodedRequestValue(self):
        value = self.createRequestValue()
        if self.objId != None:
            value.setComponentByName(
                'objId',
                univ.OctetString(self.objId).subtype(
                    implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple,3)
                )
            )
        value.setComponentByName(
            'sessionID',
            univ.OctetString(self.sessionID).subtype(
                implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 0)
            )
        )
        value.setComponentByName(
            'opName',
            univ.OctetString(self.opName).subtype(
                implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 1)
            )
        )
        value.setComponentByName(
            'objName',
            univ.OctetString(self.objName).subtype(
                implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple,2)
            )
        )

        return encoder.encode(value)