from ldap.extop import ExtendedRequest
from pyasn1.type import namedtype,univ,tag
from pyasn1.codec.ber import encoder
from pyasn1_modules.rfc2251 import LDAPString
import ldap.extop.rbac.rbac_utils as rbac

"""
    Follows the ASN.1 description of this operation

    CreateSessionRequest ::= SEQUENCE{
        requestorSessionID  [0] OCTET STRING OPTIONAL
        tenantId            [1] OCTET STRING OPTIONAL
        userIdentity        [2] OCTET STRING OPTIONAL
        authenticationToken [3] OCTET STRING OPTIONAL
        roles               [4] Roles OPTIONAL
    }

    Roles ::= SEQUENCE{
        role OCTET STRING OPTIONAL
    }
"""


class CreateSessionRequest(ExtendedRequest):
    requestName = rbac.op_map[rbac.CREATE_SESSION]

    #update default values
    def __init__(self, reqSessID="", tId="", uIdent="", authTok="", roles = []):
        self.reqSessID = reqSessID
        self.tId = tId
        self.uIdent = uIdent
        self.authTok = authTok
        self.roles = roles #array of strings

    class CreateSessionRequestValue(univ.Sequence):
        componentType = namedtype.NamedTypes(
            namedtype.NamedType('requestorSessionID',
                LDAPString().subtype(
                    implicitTag=tag.Tag(tag.tagClassContext,tag.tagFormatSimple,0)
                )
            ),
            namedtype.NamedType('tenantId',
                LDAPString().subtype(
                    implicitTag=tag.Tag(tag.tagClassContext,tag.tagFormatSimple,1)
                )
            ),
            namedtype.NamedType('userIdentity',
                LDAPString().subtype(
                    implicitTag=tag.Tag(tag.tagClassContext,tag.tagFormatSimple,2)
                )
            ),
            namedtype.NamedType('authenticationToken',
                LDAPString().subtype(
                    implicitTag=tag.Tag(tag.tagClassContext,tag.tagFormatSimple,3)
                )
            ),
            namedtype.NamedType('roles',
                univ.Sequence().subtype(
                    implicitTag=tag.Tag(tag.tagClassContext,tag.tagFormatConstructed,4)
                )
            ),
        )

    #create the component type based on the roles
    def create_component_type(self):
        role_list = ()
        for index in range(len(self.roles)):
            typed_role = (namedtype.NamedType(None,
                LDAPString(self.roles[index]).subtype(
                    implicitTag=tag.Tag(tag.tagClassContext,tag.tagFormatSimple,index)
                    )
                ),
            )
            role_list = role_list + typed_role

        componentType = namedtype.NamedTypes(*role_list)
        return componentType

    def encodedRequestValue(self):
        value = self.CreateSessionRequestValue()
        componentType = self.create_component_type()

        value.setComponentByName(
            'requestorSessionID',
            LDAPString(self.reqSessID).subtype(
                implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple,0)
            )
        )
        value.setComponentByName(
            'tenantId',
            LDAPString(self.tId).subtype(
                implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple,1)
            )
        )
        value.setComponentByName(
            'userIdentity',
            LDAPString(self.uIdent).subtype(
                implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple,2)
            )
        )
        value.setComponentByName(
            'authenticationToken',
            LDAPString(self.authTok).subtype(
                implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple,3)
            )
        )
        value.setComponentByName(
            'roles',
            univ.Sequence(componentType=componentType).subtype(
                implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatConstructed,4)
            )
        )
        #set the roles sequence
        seq = value.getComponentByPosition(4)
        for index in range(len(self.roles)):
            seq.setComponentByPosition(
                index,
                LDAPString(self.roles[index]).subtype(
                    implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple,index)
                )
            )
        return encoder.encode(value)