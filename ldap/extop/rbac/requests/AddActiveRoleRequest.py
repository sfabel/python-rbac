from ldap.extop import ExtendedRequest
from pyasn1.type import namedtype,univ,tag
from pyasn1.codec.ber import encoder
import ldap.extop.rbac.rbac_utils as rbac

"""
    Follows the ASN.1 description of this operation

    RBACAddActiveRoleRequestValue ::= SEQUENCE{
        user         [0] OCTET STRING
        session      [1] OCTET STRING
        activeRole   [2] OCTET STRING
    }
"""

class AddActiveRoleRequest(ExtendedRequest):
    requestName = rbac.op_map[rbac.ADD_ACTIVE_ROLE]

    def __init__(self,user,session,activeRole):
        self.user = user
        self.session = session
        self.activeRole = activeRole

    class AddActiveRoleRequestValue(univ.Sequence):
        componentType = namedtype.NamedTypes(
            namedtype.NamedType('user',
                univ.OctetString().subtype(
                    implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple,0)
                )
            ),
            namedtype.NamedType('session',
                univ.OctetString().subtype(
                    implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple,1)
                )
            ),
            namedtype.NamedType('activeRole',
                univ.OctetString().subtype(
                    implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 2)
                )
            )
        )

    def encodedRequestValue(self):
        value = self.AddActiveRoleRequestValue()
        value.setComponentByName(
            'user',
            univ.OctetString(self.user).subtype(
                implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 0)
            )
        )
        value.setComponentByName(
            'session',
            univ.OctetString(self.session).subtype(
                implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple, 1)
            )
        )
        value.setComponentByName(
            'activeRole',
            univ.OctetString(self.activeRole).subtype(
                implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple,2)
            )
        )
        return encoder.encode(value)