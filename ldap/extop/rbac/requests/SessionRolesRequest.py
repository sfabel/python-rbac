from ldap.extop import ExtendedRequest
from pyasn1.type import namedtype,univ,tag
from pyasn1.codec.ber import encoder
import ldap.extop.rbac.rbac_utils as rbac

"""
    Follows the ASN.1 description of this operation

    CreateSessionRequest ::= SEQUENCE{
        user        [0] OCTET STRING
        sessionID   [1] OCTET STRING
    }
"""

class SessionRolesRequest(ExtendedRequest):
    requestName = rbac.op_map[rbac.SESSION_ROLES]

    def __init__(self, user,sessionID):
        self.user = user
        self.sessionID = sessionID

    class CreateSessionRequestValue(univ.Sequence):
        componentType = namedtype.NamedTypes(
            namedtype.NamedType('user',
                univ.OctetString().subtype(
                    implicitTag=tag.Tag(tag.tagClassContext,tag.tagFormatSimple,0)
                )
            ),
            namedtype.NamedType('sessionID',
                univ.OctetString().subtype(
                    implicitTag=tag.Tag(tag.tagClassContext,tag.tagFormatSimple,1)
                )
            )
        )

    def encodedRequestValue(self):
        value = self.CreateSessionRequestValue()
        value.setComponentByName(
            'user',
            univ.OctetString(self.user).subtype(
                implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple,0)
            )
        )
        value.setComponentByName(
            'sessionID',
            univ.OctetString(self.sessionID).subtype(
                implicitTag=tag.Tag(tag.tagClassContext, tag.tagFormatSimple,1)
            )
        )
        return encoder.encode(value)