CREATE_SESSION = 1
CHECK_ACCESS = 2
ADD_ACTIVE_ROLE = 3
DROP_ACTIVE_ROLE = 4
DELETE_SESSION = 5
SESSION_ROLES = 6

op_map = {
    CREATE_SESSION: '1.3.6.1.4.1.4203.555.1',
    CHECK_ACCESS: '1.3.6.1.4.1.4203.555.2',
    ADD_ACTIVE_ROLE: '1.3.6.1.4.1.4203.555.3',
    DROP_ACTIVE_ROLE: '1.3.6.1.4.1.4203.555.4',
    DELETE_SESSION: '1.3.6.1.4.1.4203.555.5',
    SESSION_ROLES: '1.3.6.1.4.1.4203.555.6'
}

#Dynamically generate RBAC error classes
def rbac_error_factory(msg):
    #format error msg
    split = msg.split(':')
    split[1] = split[1].strip()

    #class name
    error_type = split[0].upper()
    error_class = type(error_type, (Exception,), dict(message=split[1]))
    return error_class

#Create error messages and raise them
def handle_error(ldap_error, rbac_op, args=None):
    if rbac_op == CHECK_ACCESS:
        if ldap_error.__class__.__name__=='UNWILLING_TO_PERFORM':
            #args[0] = msgid
            return (120,[],args[0],[], op_map[CHECK_ACCESS], 0)
    if rbac_op > 0:
        #for some errors rbac methods don't return an LdapError with an info field
        if 'info' in ldap_error[0].keys():
            error_msg = ldap_error[0]['info']
        else: #these assume that session key is valid
            if rbac_op == DELETE_SESSION:
                error_msg = 'rbac_delete_session: session not owned by user'
            elif rbac_op == DROP_ACTIVE_ROLE:
                error_msg = 'rbac_drop_active_role: role not assigned to session'
        error = rbac_error_factory(error_msg)
        return error
