from ldap.extop import ExtendedResponse
from pyasn1.type import namedtype,univ,tag
from pyasn1.codec.ber import decoder
from pyasn1_modules.rfc2251 import LDAPString
import ldap.extop.rbac.rbac_utils as rbac

"""
    Follows the ASN.1 description of this operation

    RBACACreateSessionResponse ::= SEQUENCE{
        sessionID [0] OCTET STRING OPTIONAL
    }

    Returns the sessionID as a string
"""

class CreateSessionResponse(ExtendedResponse):
    responseName = rbac.op_map[rbac.CREATE_SESSION]

    def __init__(self, value):
        self.responseValue = value

    class CreateSessionResponseValue(univ.Sequence):
        componentType = namedtype.NamedTypes(
            namedtype.NamedType('sessionID',
                LDAPString().subtype(implicitTag=tag.Tag(tag.tagClassContext,tag.tagFormatSimple,0)
                )
            )
        )

    def decodeResponseValue(self):
        value,_ = decoder.decode(self.responseValue,asn1Spec=self.CreateSessionResponseValue())
        sessionID = str(value.getComponentByName('sessionID'))
        return sessionID