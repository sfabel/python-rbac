from ldap.extop import ExtendedResponse
import ldap.extop.rbac.rbac_utils as rbac

"""Return False if server throws error (access denied), returns True if
    server responds with None (access granted)"""

class CheckAccessResponse(ExtendedResponse):
    responseName = rbac.op_map[rbac.CHECK_ACCESS]

    def __init__(self, value):
        self.responseValue = value

    def decodeResponseValue(self):
        if self.responseValue == None:
            return True
        else:
            return False