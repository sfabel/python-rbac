from ldap.extop import ExtendedResponse
from pyasn1.codec.ber import decoder
import ldap.extop.rbac.rbac_utils as rbac

"""
    Follows the ASN.1 description of this operation

    RBACSessionRolesResponse ::= SEQUENCE{
        roles, OCTET STRING OPTIONAL, tag 0x04U
    }

    Returns the role names that are activated within a user's session as an array
"""

class SessionRolesResponse(ExtendedResponse):
    responseName = rbac.op_map[rbac.SESSION_ROLES]

    def __init__(self, value):
        self.responseValue = value

    def decodeResponseValue(self):
        value = decoder.decode(self.responseValue)[0]

        roles = []
        for index in range(len(value)):
            role = value.getComponentByPosition(index)
            roles.append(str(role))
        return roles